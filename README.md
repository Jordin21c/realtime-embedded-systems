<center> <h1>Realtime Embedded Systems (TINRES01)</h1> </center>

<center> <h2>Opdracht 1: BlinkingLight</h2> </center>
1. run /src/blinkingLight/world.py and show it is working.

Controls:
* Left mouse click/enter to go into edit mode
* Right mouse click/ESC to back to released mode where values are loaded
* PAGE UP/PAGE DOWN to change pages
* Wheel press makes 1 -> 0 without freezing
* Wheel scroll change value of field without freezing


<center> <h2>Opdracht 2: TrafficLights</h2> </center>

<center> <h4>Onderzoek de werking van 4 modi en de straatlantaarn</h4> </center>
Verschillende modi zijn te vinden in Mode switching
Everytime the modeButton is being hit, a modepulse 

```python
self.part ('Mode switching')
self.modePulse.trigger (self.modeButton)
self.modeStep.set ((self.modeStep + 1) % 4, self.modePulse)
self.regularMode.mark (self.modeStep == 0)
self.cycleMode.mark (self.modeStep == 1)
self.nightMode.mark (self.modeStep == 2)
self.offMode.mark (self.modeStep == 3)
```


  1. When modeStep == 0 -> regularMode:
The north and south traffic light will blink and go on/off in sync and in reverse of west and east traffic light.
    	
```python
self.part ('Regular mode phases')
self.northSouthGreen.mark (0 < self.regularPhaseTimer < self.tNorthSouthGreen)
self.northSouthBlink.mark (self.tNorthSouthGreen < self.regularPhaseTimer < self.tNorthSouthBlink)
self.eastWestGreen.mark (self.tNorthSouthBlink < self.regularPhaseTimer < self.tEastWestGreen)
self.eastWestBlink.mark (self.tEastWestGreen < self.regularPhaseTimer)' 	
```
  
  2. When modeStep == 1 -> cycleMode:
Cycle through traffic lights clockwise. Green->Blinking->Red. Starts at North traffic light.
   
```python 
self.part ('Cycle mode phases')
self.northGreen.mark (0 < self.cyclePhaseTimer < self.tNorthGreen)
self.northBlink.mark (self.tNorthGreen < self.cyclePhaseTimer < self.tNorthBlink)
self.eastGreen.mark (self.tNorthBlink < self.cyclePhaseTimer < self.tEastGreen)
self.eastBlink.mark (self.tEastGreen < self.cyclePhaseTimer < self.tEastBlink)
self.southGreen.mark (self.tEastBlink < self.cyclePhaseTimer < self.tSouthGreen)
self.southBlink.mark (self.tSouthGreen < self.cyclePhaseTimer < self.tSouthBlink)
self.westGreen.mark (self.tSouthBlink < self.cyclePhaseTimer < self.tWestGreen)
self.westBlink.mark (self.tWestGreen < self.cyclePhaseTimer)
```
  
  3. When modeStep == 2 -> nightMode:
All traffic lights are blinking red. Because nightMode is True and blink will be True with tBlink time. 
```python
self.part ('Night blinking')
self.allowRed.mark (self.regularMode or self.cycleMode or (self.nightMode and self.blink))
```
        
  4. When modeStep == 3 -> offMode:

Nothing will happen.

<center> <h5>Straatlantaarn:</h5> </center>
When the brightButton is being toggled the the street illumination will be turned on/off.

```python
self.part ('Street illumination')
self.brightPulse.trigger (self.brightButton)
self.brightDirection.mark (not self.brightDirection, self.brightPulse)
self.brightDelta.set (-self.brightFluxus * world.period, self.brightDirection, self.brightFluxus * world.period)
self.streetLamp.set (limit (self.streetLamp + self.brightDelta, self.brightMin, self.brightMax), self.brightButton)
```
<center> <h4>Voorzie iedere regel van de sweep-functie in
trafficLights.py van commentaar dat de functie van
die regel kort uitlegt</h4> </center>

See source code /src/trafficLights/trafficLights.py

<center> <h2>Opdracht 3: OneArmedRobot</h2> </center>
Verbeter de besturing van een robotarm:

* Bestudeer het voorbeeldprogramma oneArmedRobot. De besturing van de robotarm (vooral de torso) vertoont veel overshoot.
* Geef aan wat het probleem is:
    * hand and finger servos:
        * De finAng.set is niet correct geprogrammeerd tenzij de vingers van een buigzame materiaal zijn. Als het materiaal metaal zou zijn dan mag de hoek niet groter dan 15 graden zijn.
    * Remweg van het object wordt niet meegenomen, dit zorgt ervoor dat het object pas tegen"gas" gaat geven nadat het het desbetreffende punt heeft bereikt. En hierdoor vervolgens steeds heen en weer schommelt.
* Ontwerp en implementeer een verbeterde besturing die rekening houdt met de afremafstand.
    * 2 mogelijkheden om het mogelijk op te lossen:
        * Mogelijkheid om te remmen/tegengas geven zodra remweg is bereikt.
            * Voordelen:
                * A naar B het snelst.
            * Nadelen: 
                * Gecompliceerder dan 2e oplossing:
            
        * Minder gas geven naarmate de punt dichterbij komt.
            * Voordelen:
                * Minder gecompliceerd.
            * Nadelen: 
                * Zorgt er niet voor dat A naar B het snelst is.
* Zie het HowTo-document voor details.

<center> <h2>Opdracht 4: Eindopdracht wasmachine</h2> </center>
See source code: src/final_assignment(opdracht4)