# ====== Legal notices
#
# Copyright(C) 2013  - 2018 GEATEC engineering
#
# This program is free software.
# You can use, redistribute and/or modify it, but only under the terms stated in the QQuickLicence.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY, without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the QQuickLicence for details.
#
# The QQuickLicense can be accessed at: http://www.qquick.org/license.html
#
# __________________________________________________________________________
#
#
#  THIS PROGRAM IS FUNDAMENTALLY UNSUITABLE FOR CONTROLLING REAL SYSTEMS !!
#
# __________________________________________________________________________
#
# It is meant for training purposes only.
#
# Removing this header ends your licence.
#

from SimPyLC import *

class washingMachine(Module):
    def __init__(self):
        Module.__init__(self)

        self.page('Washing Machine')

        self.group ('Washing Machine controls', True)
        self.startButton = Marker()
        self.programButton = Marker()
        self.desiredTemp = Register()
        self.preWashButton = Marker()
        self.doorButton = Marker()

        self.group('Triggers')
        self.startButtonTrigger = Oneshot()
        self.preWashButtonTrigger = Oneshot()
        self.waterPumpDoneTrigger = Oneshot()
        self.waterTapDoneTrigger = Oneshot()
        self.flushTrigger = Oneshot()
        self.doorButtonTrigger =  Oneshot()
        self.preWashDoneTrigger = Oneshot()
        self.washDoneTrigger = Oneshot()

        self.group('Program switching')
        self.programPulse = Oneshot()
        self.programStep = Register()
        self.cottonProgram = Marker(True)
        self.woolProgram = Marker()
        self.silkProgram = Marker()
        self.syntheticProgram = Marker()

        self.group('Washing machine States', True)
        self.startProgram = Marker()
        self.preWash = Marker()
        self.startPreWash = Marker()
        self.preWashDone = Marker()
        self.startWash = Marker()
        self.washDone = Marker()
        self.programDone = Latch()
        self.doorOpen = Marker()
        self.preWashCompartment = Latch()
        self.washCompartment = Latch()
        self.waterTap = Marker()
        self.waterTapDone = Latch()
        self.heatingWater = Marker()
        self.heatingWaterDone = Latch()
        self.drum = Marker()
        self.drumTiming = Timer()
        self.drumSpeed = Register(0)
        self.drumDone = Latch()
        self.waterPump = Marker()
        self.waterPumpDone = Latch()
        self.flush = Latch()
        self.waterTapFlushDone = Latch()
        self.drumFlushDone = Latch()
        self.waterPumpFlushDone = Latch()
        self.flushDone = Latch()
        self.centrifuge = Marker()
        self.centrifugeDone = Latch()
        self.currentTempWater = Register(15)
        self.deltaTempWater = Register()
        self.currentQuantityWater = Register(0)
        self.maxWaterCap = Register(10)

        self.group('Static Values',True)
        self.minTemp = Register(40)
        self.maxTemp = Register(95)
        self.maxTempCottonSynthetic = Register(95)
        self.maxTempWool = Register(60)
        self.maxTempSilk = Register(40)
        self.maxWaterVolumePerSec = Register(1)

        self.group('delay')
        self.delayTimer = Timer()
        self.delayDone = Latch()
        self.delayTrigger = Oneshot()
        self.delay = Marker()

        self.group('World Time')
        self.multiplyTime = Register(1)

        self.group('System runner')
        self.run = Runner()


    def input(self):
        pass

    def sweep(self):
        self.part('Start button')
        self.startButtonTrigger.trigger(self.startButton)
        self.startProgram.mark(not self.startProgram, self.startButtonTrigger and 0<=self.programStep<=3)

        self.part('Program switching')
        self.programPulse.trigger(self.programButton)
        self.programStep.set((self.programStep + 1) % 4, self.programPulse)
        self.cottonProgram.mark (self.programStep == 0)
        self.woolProgram.mark (self.programStep == 1)
        self.silkProgram.mark (self.programStep == 2)
        self.syntheticProgram.mark (self.programStep == 3)

        self.part('Max temperature based on program')
        self.maxTemp.set(self.maxTempCottonSynthetic, self.cottonProgram or self.syntheticProgram)
        self.maxTemp.set(self.maxTempWool, self.woolProgram)
        self.maxTemp.set(self.maxTempSilk, self.silkProgram)

        self.part('Temperature button')
        self.desiredTemp.set(limit(self.desiredTemp, self.minTemp, self.maxTemp))

        self.part('Pre-wash button')
        self.preWashButtonTrigger.trigger(self.preWashButton)
        self.preWash.mark(not self.preWash, self.preWashButtonTrigger)

        self.part('Door control')
        self.doorButtonTrigger.trigger(self.doorButton)
        self.doorOpen.mark(not self.doorOpen, self.doorButtonTrigger and self.currentQuantityWater<=0 and not self.drum)

        self.part('Water tap control')
        self.waterTap.mark(True, ((self.startPreWash and self.preWashCompartment) or (self.startWash and self.washCompartment)) and ((not self.waterTapDone and not self.flush) or (not self.waterTapFlushDone and self.flush)) and not self.programDone and not self.doorOpen, False)
        self.currentQuantityWater.set(round((self.currentQuantityWater+((world.period*self.multiplyTime) * self.maxWaterVolumePerSec)),2), self.waterTap)
        self.waterTapDoneTrigger.trigger(self.currentQuantityWater>=self.maxWaterCap and self.waterTap)
        self.waterTapDone.latch(self.waterTapDoneTrigger and not self.flush)
        self.waterTapFlushDone.latch(self.waterTapDoneTrigger and self.flush)

        self.part('Water heating control')
        self.heatingWater.mark(True, self.waterTapDone and not self.flush and not self.heatingWaterDone, False)
        self.deltaTempWater.set(round((self.desiredTemp - self.currentTempWater),2), self.heatingWater, False)
        # TODO BUG within program itself, it doesn't catch devided by zero exceptions
        if self.currentQuantityWater:
            self.currentTempWater.set((self.currentTempWater+(0.36*(world.period*self.multiplyTime)/self.currentQuantityWater)),self.heatingWater)
        self.currentTempWater.set(15, self.currentQuantityWater<=0 or self.waterPumpDoneTrigger)
        self.heatingWaterDone.latch(self.currentTempWater >= self.desiredTemp and self.heatingWater)

        self.part('Drum control')
        self.drum.mark(True, (self.heatingWaterDone and not self.flush and not self.drumDone) or (self.flush and self.waterTapFlushDone and not self.drumFlushDone) or self.centrifuge,False)
        self.drumSpeed.set(0, not self.drum)
        self.drumSpeed.set(10, self.drum and (self.startPreWash or self.silkProgram or self.syntheticProgram or self.flush))
        self.drumSpeed.set(20, self.drum and (self.cottonProgram or self.woolProgram) and not self.startPreWash and not self.flush)
        self.drumSpeed.set(100, self.drum and self.centrifuge)
        self.drumDone.latch(self.drumTiming>= (60/self.multiplyTime) and self.startPreWash and self.drum)
        self.drumDone.latch(self.drumTiming>= (120/self.multiplyTime) and (self.woolProgram or self.silkProgram) and self.drum)
        self.drumDone.latch(self.drumTiming>= (180/self.multiplyTime) and (self.cottonProgram  or self.syntheticProgram) and self.drum)
        self.drumFlushDone.latch(self.drumTiming>= (120/self.multiplyTime) and (self.flush and self.waterTapFlushDone) and self.drum)
        self.centrifugeDone.latch(self.drumTiming>= (120/self.multiplyTime) and self.centrifuge and self.drum)
        self.drumTiming.reset(not self.drum)

        self.part('Water pump control')
        self.waterPump.mark(True, (self.drumDone and not self.waterPumpDone) or (self.drumFlushDone and not self.waterPumpFlushDone), False)
        self.currentQuantityWater.set(round(self.currentQuantityWater-((world.period*self.multiplyTime) * self.maxWaterVolumePerSec),2), self.waterPump)
        self.waterPumpDoneTrigger.trigger(self.currentQuantityWater<=0 and self.waterPump)
        self.waterPumpDone.latch(self.waterPumpDoneTrigger and not self.flush)
        self.waterPumpFlushDone.latch(self.waterPumpDoneTrigger and self.flush)

        self.part('Flush machine')
        self.flushTrigger.trigger(self.waterPumpDone and not self.flush)
        self.flush.latch(self.flushTrigger)
        self.flushDone.latch(self.waterPumpFlushDone and self.flush)

        self.part('Prewash program')
        self.startPreWash.mark(True, self.preWash and self.startProgram, False)
        self.preWashCompartment.latch(self.startPreWash and not self.preWashCompartment)
        self.preWashDoneTrigger.trigger(self.flushDone and self.preWash and not self.preWashDone)
        self.preWashDone.mark(True, self.preWashDoneTrigger)

        self.part('Washing program')

        self.startWash.mark(True, ((self.preWash and self.preWashDone and self.delayDone) or (not self.preWash and self.startProgram)) and not self.washDone , False)
        self.washCompartment.latch(self.startWash and not self.washCompartment)
        self.washDoneTrigger.trigger(self.flushDone and self.startWash)
        self.washDone.mark(True, self.washDoneTrigger)

        self.part('Centrifuge')
        self.centrifuge.mark(self.washDone and not self.centrifugeDone and not self.startWash)
        self.programDone.latch(self.centrifugeDone)

        self.delayTimer.reset(not self.preWashDone or self.delayDone)
        self.delayTrigger.trigger(self.delayTimer>=2)
        self.delayDone.latch(self.delayTrigger)

        self.part('Reset latches')
        self.waterTapDone.unlatch(self.preWashDoneTrigger or self.washDone)
        self.heatingWaterDone.unlatch(self.preWashDoneTrigger or self.washDone)
        self.drumDone.unlatch(self.preWashDoneTrigger or self.washDone)
        self.waterPumpDone.unlatch(self.preWashDoneTrigger or self.washDone)
        self.flush.unlatch(self.preWashDoneTrigger or self.washDone)
        self.waterTapFlushDone.unlatch(self.preWashDoneTrigger or self.washDone)
        self.drumFlushDone.unlatch(self.preWashDoneTrigger or self.washDone)
        self.waterPumpFlushDone.unlatch(self.preWashDoneTrigger or self.washDone)
        self.flushDone.unlatch(self.preWashDoneTrigger or self.washDone)







