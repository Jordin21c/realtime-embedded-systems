# ====== Legal notices
#
# Copyright (C) 2013 - 2018 GEATEC engineering
#
# This program is free software.
# You can use, redistribute and/or modify it, but only under the terms stated in the QQuickLicence.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY, without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the QQuickLicence for details.
#
# The QQuickLicense can be accessed at: http://www.qquick.org/license.html
#
# __________________________________________________________________________
#
#
#  THIS PROGRAM IS FUNDAMENTALLY UNSUITABLE FOR CONTROLLING REAL SYSTEMS !!
#
# __________________________________________________________________________
#
# It is meant for training purposes only.
#
# Removing this header ends your licence.
#

from SimPyLC import *

class Timing (Chart):
    def __init__ (self):
        Chart.__init__ (self)

    def define (self):
        self.channel (world.washingMachine.startProgram, green, 0,1,50)
        self.channel (world.washingMachine.currentQuantityWater, blue, 0,20,50)
        self.channel (world.washingMachine.currentTempWater, red, 0,95,50)
        self.channel (world.washingMachine.drumSpeed, green, 0,20,50)

        self.group('States')
        self.channel (world.washingMachine.waterTap, blue, 0,1,50)
        self.channel (world.washingMachine.heatingWater, red, 0,1,50)
        self.channel (world.washingMachine.drum, green, 0,1,50)
        self.channel (world.washingMachine.waterPump, blue, 0,1,50)
        self.channel (world.washingMachine.flush, blue, 0,1,50)
        self.channel (world.washingMachine.delayTimer, blue, 0,5,50)




