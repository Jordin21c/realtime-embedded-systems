from SimPyLC import *

class Demo (Module):
    def __init__ (self):
        Module.__init__ (self)
        self.page ('Control')
        self.group ('Elements', True)
        self.blinkingTimer = Timer ()
        self.blinkingLight = Marker ()
        self.run = Runner ()
        
    def input (self):   
        pass
    
    def sweep (self):
        self.blinkingTimer.reset (self.blinkingTimer > 2)
        self.blinkingLight.mark (self.blinkingTimer > 1)
        
