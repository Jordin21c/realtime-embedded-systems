from SimPyLC import *

class Timing (Chart):
    def __init__ (self):
        Chart.__init__ (self)
        
    def define (self):
        self.channel (world.demo.blinkingTimer, red, 0, 2, 50)
        self.channel (world.demo.blinkingLight, lime)
        
