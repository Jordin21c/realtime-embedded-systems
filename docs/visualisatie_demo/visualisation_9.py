from SimPyLC import *

class Visualisation (Scene):
    def __init__ (self):
        Scene.__init__ (self)
        self.car = Beam(size = (1, 2, 1), color = (0, 0, 1), pivot = (0, -1, 1))
        self.light = Cylinder(size = (0.2, 0.2, 0.2), color = (1, 1, 0), center = (0, -0.5, 0.6))
        self.wheel = Cylinder(size = (1, 1, 0.2), color = (0, 1, 0), axis = (0, 1, 0), angle = 90)
        self.turnbase = Cylinder(size = (0.5, 0.5, 0.2), center = (0, 0.75, 0.6), color = (1, 0, 0), pivot = (0, 0, 1))
        self.arm = Beam(size = (0.2, 2, 0.2), color = (1, 0, 0), center = (0, 1, 0), pivot = (1, 0, 0), joint = (0, -1, 0))
        self.joint = Cylinder(size = (0.3, 0.3, 0.3), center = (0, 1, 0), color = (1, 0, 0), axis = (0, 1, 0), angle = 90)
     
    def display (self):
        t1 = world.demo.blinkingTimer
        if t1 > 1:
            t2 = 2 - t1
        else:
            t2 = t1
        if t2 < 0.5:
            a = t2 * 120 - 30
            b = 30
        else:
            b = t2 * 60
            a = 30
        self.car (shift = (0, -2, 0), angle = -30, parts = lambda : 
            self.wheel (shift = (0.65, 0.8, -0.4)) + 
            self.wheel (shift = (-0.65, 0.8, -0.4)) + 
            self.wheel (shift = (0.65, -0.8, -0.4)) + 
            self.wheel (shift = (-0.65, -0.8, -0.4)) + 
            self.light (color = (1, 1, 0) if world.demo.blinkingLight else (0.1, 0.1, 0)) + 
            self.turnbase (angle = a, parts = lambda: 
                self.arm (angle = b, parts = lambda: 
                    self.joint(parts = lambda: 
                        self.arm (angle = -2 * b)))))
