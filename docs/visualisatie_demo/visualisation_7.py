from SimPyLC import *

class Visualisation (Scene):
    def __init__ (self):
        Scene.__init__ (self)
        self.car = Beam(size = (1, 2, 1), color = (0, 0, 1), pivot = (0, -1, 1))
        self.light = Cylinder(size = (0.2, 0.2, 0.2), color = (1, 1, 0), center = (0, -0.5, 0.6))
        self.wheel = Cylinder(size = (1, 1, 0.2), color = (0, 1, 0), axis = (0, 1, 0), angle = 90)

    def display (self):
        self.car (angle = -30, parts = lambda : 
            self.wheel (shift = (0.65, 0.8, -0.4)) + 
            self.wheel (shift = (-0.65, 0.8, -0.4)) + 
            self.wheel (shift = (0.65, -0.8, -0.4)) + 
            self.wheel (shift = (-0.65, -0.8, -0.4)) + 
            self.light (color = (1, 1, 0) if world.demo.blinkingLight else (0.1, 0.1, 0)))
