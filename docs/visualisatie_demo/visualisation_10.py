from SimPyLC import *

class Wheel (Cylinder):
    def __init__(self, center = (0, 0, 0)):
        Cylinder.__init__ (self, size = (1, 1, 0.2), center = center, color = (0, 1, 0), axis = (0, 1, 0), angle = 90, pivot = (1, 0, 0))
        self.spoke = Beam(size = (0.3, 0.1, 0.9), color = (0, 1, 0), pivot = (1, 0, 0))

    def __call__(self, shift = (0, 0, 0), angle = 0):
        return Cylinder.__call__(self, shift = shift, angle = angle, parts = lambda: self.spoke() + self.spoke(angle = 90))

class Visualisation (Scene):
    def __init__ (self):
        Scene.__init__ (self)

        self.car = Beam(size = (1, 2, 1), color = (0, 0, 1), pivot = (0, -1, 1))
        self.turnbase = Cylinder(size = (0.5, 0.5, 0.2), center = (0, 0.75, 0.6), color = (1, 0, 0), pivot = (0, 0, 1))
        self.arm = Beam(size = (0.2, 2, 0.2), color = (1, 0, 0), center = (0, 1, 0), pivot = (1, 0, 0), joint = (0, -1, 0))
        self.wheelFront = Wheel(center = (0.65, 0.8, -0.4))
        self.wheelRear = Wheel(center = (0.65, -0.8, -0.4))
        self.light = Cylinder(size = (0.2, 0.2, 0.2), color = (1, 1, 0), center = (0, -0.5, 0.6))
        self.joint = Cylinder(size = (0.3, 0.3, 0.3), center = (0, 1, 0), color = (1, 0, 0), axis = (0, 1, 0), angle = 90)
        
    def display (self):
        t1 = world.demo.blinkingTimer
        if t1 > 1:
            t2 = 2 - t1
        else:
            t2 = t1
        if t2 < 0.5:
            a = t2 * 120 - 30
            b = 30
        else:
            b = t2 * 60
            a = 30
        self.car (shift = (0, -2, 0), angle = -30, parts = lambda:
            self.wheelFront (angle = -45 * t1) +
            self.wheelFront (shift = (-1.3, 0, 0), angle = -45 * t1) +
            self.wheelRear (angle = -45 * t1) +
            self.wheelRear (shift = (-1.3, 0, 0), angle = -45 * t1) +
            self.light (color = (1, 1, 0) if world.demo.blinkingLight else (0.1, 0.1, 0)) +
            self.turnbase (angle = a, parts = lambda:
                self.arm (angle = b, parts = lambda:
                    self.joint(parts = lambda:
                        self.arm (angle = -2 * b)))))
