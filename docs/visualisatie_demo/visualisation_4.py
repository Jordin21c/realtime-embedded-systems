from SimPyLC import *

class Visualisation (Scene):
    def __init__ (self):
        Scene.__init__ (self)
        self.car = Beam(size = (1, 2, 1), color = (0, 0, 1), axis = (0, -1, 1), angle = -30)
        self.light = Cylinder(size = (0.2, 0.2, 0.2), color = (1, 1, 0), center = (0, -0.5, 0.6))

    def display (self):
        self.car ()
        self.light ()
